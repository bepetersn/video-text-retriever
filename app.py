#!/usr/bin/env python

import sys
from py import create_app
from py.cli import cli, custom_run_command as run_command

app = create_app()

if __name__ == '__main__':
    if len(sys.argv) == 1:
        run_command.main()
    else:
        cli.main()
