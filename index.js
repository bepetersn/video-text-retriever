
// libs
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import React from 'react';
import ReactDOM from 'react-dom';
import '@fortawesome/fontawesome-free';
import '@fortawesome/fontawesome-free/css/all.css';

// our app
import App from './components/app.js';
import './scss/style.scss'

// hack for bootstrap's sake
window.$ = window.jQuery = $;

ReactDOM.render(
  <App />,
  document.getElementById('app')
);

