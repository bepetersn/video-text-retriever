## Setting up

### System requirements
    
- python >= 3.6 (Might work with like 3.4 but I haven't tested)
    
### Package requirements (via brew, apt, dnf, etc.):
    
    apt install npm
    apt install python3-pip
    apt install ffmpeg
    
### Python reqs
    pip3 install pipenv
    pipenv install
    
### Node reqs
    npm install


## Running

In one terminal, run the following commands:

    npm run build
    pipenv shell
    sudo $(which gunicorn) app:app -b 127.0.0.1:5000    

Alternatively, you can run the following:

    npx run watch

And in the first terminal, use the following, in addition to the 
line which starts with `pipenv`: 

    ./app.py # or "flask run" if you need to control port, host, etc.  


## Note: This app is not ready for production usage yet

A big reason, besides the fact that there a few bugs and more tests needed,
is that the core APIs / functionality is not asnyc / worker-based, and could
easily fail (badly) under heavy load.
    
    
## Updating the VTR's YoutubeDL dependency

This is important to do periodically if using this because Youtube changes a 
lot and breaks its own internal APIs, and the 
[YoutubeDL library](https://github.com/ytdl-org/youtube-dl), 
which this project relies on heavily on, has to make last-minute updates to 
accomodate these internal API changes. 

