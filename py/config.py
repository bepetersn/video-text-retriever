
import os, logging

class AppConfig():

    DATABASE_URI = os.environ['DATABASE_URI']
    LOG_LEVEL = os.environ.get('APP_LOG_LEVEL', logging.INFO)
    LOG_FILENAME = 'app.log'
    LOG_MAX_BYTES = 1000 * 1000 * 5 # 5MB
