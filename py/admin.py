
from datetime import date

from flask_secure_admin import SecureAdminBlueprint
from flask_admin.model import typefmt
import pytz
from pytz import timezone

MOUNTAIN_TIME = 'US/Mountain'
DATETIME_FMT = '%Y-%m-%d %-I:%M%p'


def date_format(view, value):
    utc_date = pytz.utc.localize(value)
    utc_date.astimezone(timezone(MOUNTAIN_TIME))
    return value.strftime(DATETIME_FMT)

admin_formatters = dict(typefmt.BASE_FORMATTERS)
admin_formatters.update({date: date_format})


admin = SecureAdminBlueprint(
    url_prefix='/secure-admin',
    name='Video Text Retriever',
    models=['request_logs'],
    view_options=[
        dict(
            column_list=('media_type', 'url', 'host', 'format', 'extra',
                         'ip_address', 'time_created'),
            column_filters = ('ip_address', 'url', 'host', 'media_type',
                              'format', 'time_created', 'extra'),
            column_default_sort = ('time_created', True),
            can_edit=False, can_create=False,
            column_type_formatters=admin_formatters)])
