
import os

from flask.cli import FlaskGroup, run_command, shell_command
import click

from py import create_app


cli = FlaskGroup(add_default_commands=False, create_app=create_app)
cli.add_command(shell_command)


@cli.command('run', short_help="Our custom way of running development server.")
@click.option("--host", "-h", default=None, help="The interface to bind to.")
@click.option("--port", "-p", default=5000, help="The port to bind to.")
def custom_run_command(host, port):
    """ Run development server, setting host to
        the broadcast ip address 0.0.0.0 if not
        specified otherwise and running in production,
        else to localhost. """
    if not host:
        host = ('0.0.0.0' if os.environ['FLASK_ENV'] == 'production'
                          else 'localhost')
    run_command.main(['-h', host, '-p', port])
