
import logging
import logging.handlers as handlers
from flask.logging import default_handler as flaskDefaultHandler
from py.db_logger import DatabaseHandler, insert_log
from functools import partial

def configure_logging(app):

    # Setup logger & handlers
    logging.basicConfig(level=getattr(logging, app.config['LOG_LEVEL']))
    log = logging.getLogger()
    log.addHandler(handlers.RotatingFileHandler(
            app.config['LOG_FILENAME'], maxBytes=app.config['LOG_MAX_BYTES'])) # 5MB
    log.addHandler(flaskDefaultHandler)
    log.addHandler(DatabaseHandler())

    # Monkey-patch on a new method, for logging at the
    # INSERT_LEVEL, which is handled specially by the
    # DatabaseHandler class.
    log.insert = partial(insert_log, log)
    return log
