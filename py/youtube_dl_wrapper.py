
import io, re, os, subprocess, copy, logging, sys
from contextlib import redirect_stdout, contextmanager

import iso639
import pycountry
from youtube_dl import YoutubeDL
from youtube_dl.version import __version__
from youtube_dl.postprocessor import FFmpegSubtitlesConvertorPP
from funcy import lmap
from munch import Munch

SUB_FILENAME_REGEX = 'Writing video subtitles to: (.*\.(vtt|srt))'
AV_FILENAME_REGEX = f'\] Destination: (.*?\.(mp4|mkv|srt|vtt|mp3|wav|flac))'
ALREADY_DOWNLOADED_FILENAME_REGEX = f'\[download\] (.*\.(mp4|mkv|srt|vtt|mp3|wav|flac)) has already been downloaded'
LANGUAGE_OPTION = '([a-z]{2,3}-?[a-zA-Z]*) {2,6}.*'
AUTOMATIC_CAPTIONS = 'Available automatic captions'
SUBTITLES = 'Available subtitles'
LINES_FROM_CAPTIONS_TO_LANGUAGES_START = 2
GREEK = 'Greek'
PRE_PROCESSED_FILENAME_REGEX = "(.*?)\.([a-zA-Z\-]{2,8}).(srt|vtt)"
FFMPEG_CONVERT_MP4_TO_MKV = lambda mp4path, mkvpath: \
    ['ffmpeg', '-i', mp4path, '-codec', 'copy', mkvpath, '-y']

log = logging.getLogger()


class TeeIO():
    def __init__(self, original, target):
        self.original = original
        self.target = target

    def write(self, b):
        self.original.write(b)
        self.target.write(b)

    def flush(self):
        #  ytdl uses this
        pass

@contextmanager
def tee_stdout(target):
    tee = TeeIO(sys.stdout, target)
    with redirect_stdout(tee):
        yield


def get_preprocessed_filename(filename):
    preprocessed = re.match(
       PRE_PROCESSED_FILENAME_REGEX, filename)
    if preprocessed:
        return f'{preprocessed.group(1)}.{preprocessed.group(2)}'


class YoutubeDLWrapper():

    version = __version__

    def __init__(self, run_dir=None, debug=False):
        self.run_dir = run_dir
        self.debug = debug
        self.defaults = dict(
            cachedir=False,
            outtmpl=f'{run_dir}/%(title)s-%(id)s.%(ext)s'
        )

    def pre_process(self, options):

        options['mkv_requested'] = False
        if options.get('format') == 'mkv':
            options['mkv_requested'] = True
            options['format'] = 'mp4'

        postprocessors = options.get('postprocessors')
        if postprocessors and postprocessors[0]['preferredcodec'] == 'flac':
            options['postprocessor_args'] = ['-ac', '1']

    def download(self, video_url, options, post_process=True):

        all_options = copy.deepcopy(self.defaults)
        all_options.update(options)
        self.pre_process(all_options)

        # Hide stdout unless debugging; either way, capture it
        captured = io.StringIO()
        handle_stdout = redirect_stdout if \
                        log.getEffectiveLevel() != logging.DEBUG else \
                        tee_stdout

        with handle_stdout(captured):
            with YoutubeDL(all_options) as ydl:
                log.info('downloading')
                rv = ydl.download([video_url])
                log.info('finished downloading')

        captured.seek(0)
        output = captured.read()

        result = Munch(rv=rv, output=output, video_url=video_url)
        return self.post_process(ydl, result, all_options, post_process)

    def post_process(self, ydl, result, options, post_process):

        if post_process:
            # If this is an options request, just do that
            if options.get('listsubtitles'):
                languages = self.parse_language_options(result.output)
                if languages:
                    result.languages = languages

            # Otherwise, download some stuff
            else:
                 result.filename = os.path.basename(
                    self.parse_filename(result.output))
                 if result.filename:
                     result.filename = self.convert_subtitles(
                            ydl, result, options)

                     if options['mkv_requested']:
                         result.filename = self.convert_to_mkv(
                            result, options
                         )
        return result

    def convert_to_mkv(self, result, options):

        error = None
        mp4path = os.path.join(self.run_dir, result.filename)
        mkvpath = mp4path.rsplit('.', 1)[0] + '.mkv'

        log.info('converting MP4 to MKV')
        try:
            command = FFMPEG_CONVERT_MP4_TO_MKV(mp4path, mkvpath)
            ffmpeg = subprocess.Popen(
                command, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
            stdout, stderr = ffmpeg.communicate()
        except Exception as e:
            error = e
        finally:
            if error:
                log.error('MKV conversion via ffmpeg failed')
            if self.debug or error:
                log.error(err)

        return os.path.basename(mkvpath)

    def convert_subtitles(self, ydl, result, options):
        filename = result['filename']
        if 'convertsubtitles' not in options.keys():
            return filename
        else:
            log.info('converting subtitles')
            requested_sub_format = options['convertsubtitles']
            if filename.endswith(requested_sub_format):
                return filename
            try:
                info = ydl.extract_info(result['video_url'])
                preprocessed_filename = get_preprocessed_filename(filename)

                info['filepath'] = \
                    os.path.join(self.run_dir, preprocessed_filename)
                pp = FFmpegSubtitlesConvertorPP(
                    format=requested_sub_format,
                    downloader=ydl)
                _, _ = pp.run(info)
                # pre-conversion file is deleted
                os.remove(os.path.join(self.run_dir, filename))
                return os.path.basename(
                    info['filepath'] + f'.{requested_sub_format}')
            except Exception as e:
                print('Subtitle conversion via youtube-dl failed')
                print(e)

    @staticmethod
    def parse_filename(result):

        match = re.search(SUB_FILENAME_REGEX, result)
        if match:
            return match.group(1)

        matches = re.findall(AV_FILENAME_REGEX, result)
        if matches:
            # Take the last found "Destination"
            match = matches[-1]
            return match[0]

        # "Already downloaded" may appear when it is not the
        # final filename in the case of extracting audio,
        # so it's important this come last
        match = re.search(ALREADY_DOWNLOADED_FILENAME_REGEX, result)
        if match:
            return match.group(1)

        return None

    @staticmethod
    def _get_language_names(lang_code):
        language = iso639.find(iso639_1=lang_code)
        if language:
            language = Munch(language)
            lang_name = language.name
        elif '-' in lang_code:
            lang_code, country_code = lang_code.split('-')
            language = iso639.find(iso639_1=lang_code)
            if language:
                language = Munch(language)
                lang_name = language.name
            else:
                lang_name = 'Unknown'
            country = pycountry.countries.get(
                             alpha_2=country_code.upper())
            if country:
                country_name = country.name
            else:
                country_name = 'Unknown'
            lang_name = f'{lang_name} - {country_name}'
        else:
            lang_name = 'Unknown'

        lang_names = lmap(
            lambda ln: ln.strip(), lang_name.split(';'))
        return lang_names

    @staticmethod
    def parse_language_options(output):
        subtitles_started = False
        line_number_of_languages_start = 1000000
        languages = []
        for i, line in enumerate(output.split('\n')):
            if SUBTITLES in line:
                subtitles_started = True
                continue

            if AUTOMATIC_CAPTIONS in line:
                line_number_of_languages_start = i + \
                        LINES_FROM_CAPTIONS_TO_LANGUAGES_START
                continue

            if i >= line_number_of_languages_start or subtitles_started:
                match = re.match(LANGUAGE_OPTION, line)
                if match:
                    if not subtitles_started:
                        type = 'auto'
                    else:
                        type = 'manual'

                    lang_code = match.group(1)
                    if lang_code:
                        lang_names = YoutubeDLWrapper._get_language_names(lang_code)

                    # Hack to make Greek show up more briefly
                    if GREEK in lang_names:
                        long_name, short_name = lang_names
                        lang_names = [short_name, long_name]

                    result = dict(
                        type=type,
                        code=lang_code,
                        names=lang_names
                    )
                    if result['names'] == 'Unknown':
                        print(f'Unknown lang code: {lang_code}')
                    languages.append(result)
                else:
                    pass

        return languages
