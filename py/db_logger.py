
import re
from logging import Handler
from flask import current_app

INSERT_LEVEL = 99

DB_LOG_FORMAT = ('Media requested by %(ip_address)s: '
                 '%(media_type)s (%(format)s) for %(url)s')

def insert_log(logger, **kwargs):
    logger.log(INSERT_LEVEL,
               DB_LOG_FORMAT % kwargs,
               extra=kwargs)


class DatabaseHandler(Handler):
    def emit(self, record):
        if record.levelno == INSERT_LEVEL:
            current_app.db.request_logs.insert(**record.__dict__)
            current_app.db.commit()
