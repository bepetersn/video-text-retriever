
import json, logging
from flask import request, jsonify, send_from_directory, abort
from urllib.parse import urlparse

from .blueprint import api, limiter, DATA_DIR
from ..youtube_dl_wrapper import YoutubeDLWrapper

log = logging.getLogger()

@api.route('/api/download/subtitles', methods=['POST'])
def get_subtitles_for_video():
    # YoutubeDL downloads to whichever
    # directory it is run from by default
    youtube_dl = YoutubeDLWrapper(run_dir=DATA_DIR)
    video_url = request.form['video_url']
    subtitle_format = request.form['subtitle_format']
    selected_langs = request.form.getlist('selected_lang')
    selected_lang = selected_langs[0]

    *lang_code_parts, caption_type = selected_lang.split('-')
    lang_code = '-'.join(lang_code_parts)

    log.insert(
        media_type='subtitles',
        ip_address=request.environ['REMOTE_ADDR'],
        url=video_url, format=subtitle_format,
        host=urlparse(video_url).netloc,
        extra=json.dumps(dict(caption_type=caption_type,
                              lang_code=lang_code))
    )

    # Determine correct args with which to call youtube-dl
    write_type = {}
    if caption_type == 'auto':
        write_type['writeautomaticsub'] = True
    elif caption_type == 'manual':
        write_type['writesubtitles'] = True

    # Same as either:
    # youtube-dl --skip-download --write-sub --sub-lang <lang_code> --convert-subs <subtitle_format> <video_url>, OR
    # youtube-dl --skip-download --write-auto-sub --convert-subs <subtitle_format> <video_url>
    result = youtube_dl.download(video_url, options=dict(
        skip_download=True,
        subtitleslangs=[lang_code],
        convertsubtitles=subtitle_format,
        **write_type
    ))

    if result.get('filename'):
        filename = result['filename']
        # TODO: delete the file after sending it, somehow
        return send_from_directory(DATA_DIR, filename, as_attachment=True)
    else:
        # We don't handle it well when
        # captions aren't found for the video
        abort(503)
