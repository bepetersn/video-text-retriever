
import os

from flask import Blueprint, current_app, request
from flask_limiter import Limiter
from flask_limiter.util import get_ipaddr

api = Blueprint('api', __name__)
limiter = Limiter(key_func=get_ipaddr)
DATA_DIR = os.path.abspath(
    os.path.join(
        os.path.dirname(
        os.path.dirname(
        os.path.dirname(__file__))), 'data'))


@api.before_app_first_request
def setup_limiter():
    limiter.init_app(current_app)


@limiter.request_filter
def ip_whitelist():
    # Don't limit on this ip address
    return request.remote_addr == "127.0.0.1"
