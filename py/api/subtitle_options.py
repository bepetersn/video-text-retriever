
import logging
from flask import request, jsonify
from urllib.parse import urlparse

from .blueprint import api, limiter
from ..youtube_dl_wrapper import YoutubeDLWrapper

log = logging.getLogger()

@api.route('/api/get/subtitles/options', methods=['POST'])
def get_subtitle_options_for_video():
    # Forms be submitting with all kinds of encodings
    form = request.get_json()
    if not form:
        form = request.form
    video_url = form['video_url']
    subtitle_format = form['subtitle_format']
    log.insert(
        media_type='subtitle_options',
        ip_address=request.environ['REMOTE_ADDR'],
        url=video_url, format=subtitle_format,
        host=urlparse(video_url).netloc)

    youtube_dl = YoutubeDLWrapper()
    # Same as youtube-dl --list-subs <video_url>
    result = youtube_dl.download(video_url, options=dict(
        skip_download=True,
        listsubtitles=True,
        subtitlesformat=subtitle_format
    ))
    if result.get('languages'):
        return jsonify(result['languages'])
    else:
        return jsonify([])
