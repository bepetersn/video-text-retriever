
import logging
from flask import request, jsonify
from urllib.parse import urlparse

from .blueprint import api, limiter
from ..youtube_dl_wrapper import YoutubeDLWrapper

log = logging.getLogger()

@api.route('/api/version/', methods=['GET'])
def get_youtube_dl_version():
    return jsonify({
        'youtube_dl': YoutubeDLWrapper.version
    })
