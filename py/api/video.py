
import logging
from flask import request, jsonify, send_from_directory, abort
from urllib.parse import urlparse

from .blueprint import api, limiter, DATA_DIR
from ..youtube_dl_wrapper import YoutubeDLWrapper

log = logging.getLogger()

@api.route('/api/download/video', methods=['POST'])
def get_video():
    youtube_dl = YoutubeDLWrapper(run_dir=DATA_DIR)
    video_url = request.form['video_url']
    video_format = request.form['video_format']
    log.insert(
        media_type='video',
        ip_address=request.environ['REMOTE_ADDR'],
        url=video_url, format=video_format,
        host=urlparse(video_url).netloc
    )

    # Same as youtube-dl -f <video_format> <video_url>
    result = youtube_dl.download(video_url, options=dict(
        format=video_format
    ))

    if result.get('filename'):
        filename = result['filename']
        # TODO: delete the file after sending it, somehow
        return send_from_directory(DATA_DIR, filename, as_attachment=True)
    else:
        # We don't handle it well when
        # captions aren't found for the video
        abort(503)
