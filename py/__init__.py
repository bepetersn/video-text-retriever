

from functools import partial

from flask import Flask, render_template, send_from_directory, request
from dotenv import load_dotenv
from sqlsoup import SQLSoup
from werkzeug.utils import secure_filename
from werkzeug.contrib.fixers import ProxyFix

from py.logging import configure_logging
from py.config import AppConfig
from py.api import api
from py.admin import admin

def create_app():

    load_dotenv()
    app = Flask(__name__, template_folder='templates')
    app.config.from_object(AppConfig())
    app.db = SQLSoup(app.config['DATABASE_URI'])
    configure_logging(app)
    app.register_blueprint(api)
    app.register_blueprint(admin)

    # Fix headers coming from upstream server usage
    # NOTE: this app MUST be run behind a proxy or
    # else this line results in a security risk
    if app.config['ENV'] == 'production':
        app.wsgi_app = ProxyFix(app.wsgi_app)

    @app.route('/<path:path>')
    def distribute(path):
        path = secure_filename(path)
        return send_from_directory('../dist', path)

    @app.route('/')
    def index():
        return send_from_directory('../dist', 'index.html')

    return app
