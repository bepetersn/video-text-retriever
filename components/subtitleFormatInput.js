
import React, { Component } from 'react';

class SubtitleFormatInput extends React.Component {
  render() {
    return (
      <input type="radio" name="subtitle_format" id="subtitle-format"
             value={this.props.value} checked={this.props.checked ? true : ''}
             onChange={this.props.onChange}/>
    )
  }
}

export default SubtitleFormatInput;
