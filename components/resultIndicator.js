
import React, { Component } from 'react';
import { titleize } from 'inflected';

class ResultIndicator extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    if(this.props.result && this.props.result !== 'success') {
      const className = 'danger';
      let explanation = null;
      if (this.props.parent == 'subtitles') {
        if (this.props.result == 'failed') {
          explanation = 'Please ensure URL is valid and you\'ve selected a language.';
        }
        if (this.props.result == 'no-options') {
          explanation = 'Unfortunately, the video you selected has no text captions available.';
        }
      }
      if (this.props.result == 'rate-limited') {
          explanation = `Unfortunately, you have requested too much ${this.props.parent} content in a certain span of time. `
          explanation += `The threshold you have reached is: ${this.props.resultDetail}.`
      }
      if (explanation === null) {
        explanation = 'Sorry, something went wrong.'
      }
      return (
        <div id="result-indicator">
          <h4 className={className}>
            {explanation}
          </h4>
        </div>
      )
    } else {
      return null;
    }
  }
}

export default ResultIndicator;
