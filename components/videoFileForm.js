import React, { Component } from 'react';
import triggerFileDownload from 'js-file-download';
import contentDisposition from 'content-disposition';

import SubtitleFormatInput from './subtitleFormatInput.js';
import LanguageOptions from './languageOptions.js';
import LoadingIndicator from './loadingIndicator.js';
import ResultIndicator from './resultIndicator.js';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const VIDEO_API = '/api/download/video'

const theme = createMuiTheme({
  palette: {
    primary: { main: '#212529' },
    secondary: { main: '#FFBD54' },
  },
});

class VideoFileForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {videoFormat: 'mp4'};
    this.handleVideoFormatChange = this.handleVideoFormatChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e){
     e.preventDefault();
     const data = new FormData(e.target);
     this.setState({ loading: true, result: null });
     this.props.setLoad(true);
     fetch(VIDEO_API, {
      method: 'POST',
      body: data
    }).then(response => {
      if(response.ok) {
        const disposition = contentDisposition.parse(response.headers.get('content-disposition'));
        const filename = disposition.parameters.filename;
        response.blob().then((content) => {
          this.setState({ loading: false });
          this.props.setLoad(false);
          triggerFileDownload(
            content, filename,
            filename.endsWith('mp4') ? "video/mp4" : "video/mkv");
        });
      } else if(response.status === 429) {
        console.log('rate limited')
        response.text().then((text) => {
          const parser = new DOMParser();
          const htmlDoc = parser.parseFromString(text, 'text/html');
          const threshold = htmlDoc.querySelector('p').textContent;
          this.setState({ loading: false,
                          result: 'rate-limited',
                          resultDetail: threshold });
          this.props.setLoad(false);
        });
      }
      else {
        console.log('failed');
        console.log(response.status);
        this.setState({ loading: false, result: 'failed', resultDetail: null });
        this.props.setLoad(false);
      }
    }).catch(response => {
      console.log('connection error');
      this.setState({ loading: false, result: 'failed', resultDetail: null });
      this.props.setLoad(false);
    });
  }

  handleVideoFormatChange(e) {
    this.setState({ videoFormat: e.target.value });
  }

  render() {
    const isMp4Checked = this.state.videoFormat == 'mp4' ? true : false;
    const isMkvChecked = this.state.videoFormat == 'mkv' ? true : false;
    const buttonDisabled = this.props.buttonDisabled ? true : false;
    return (
      <MuiThemeProvider theme = {theme}>
        <LoadingIndicator loading={this.state.loading} tabValue = {this.props.tabValue} />
        <form id='video-file-form' onSubmit={this.onSubmit}>
           <TextField
              id="video-videourl"
              className="videoUrl"
              label="Enter Video URL"
              type="url"
              margin="normal"
              variant="outlined"
              name="video_url"
              style = {{width: '50%',background:"white",borderRadius:"5px"}}

           />
          <FormLabel component="legend" id = "filetypelabel">Choose a File Type</FormLabel>
          <RadioGroup id = "filetypeselect" row name="video_format" style ={{justifyContent: "center"}}>
            <FormControlLabel
                  value="mp4"
                  control={<Radio color="primary" />}
                  checked={isMp4Checked}
                  onChange={this.handleVideoFormatChange}
                  label=".MP4"
                  labelPlacement="bottom"
                  color="primary"
            />
            <FormControlLabel
                  value="mkv"
                  control={<Radio color="primary" />}
                  checked={isMkvChecked}
                  onChange={this.handleVideoFormatChange}
                  label=".MKV"
                  labelPlacement="bottom"
                  color="primary"
            />
          </RadioGroup>
          <i className="fas fa-arrow-alt-circle-down" id = "downloadicon" />
          <input role="button" type="submit" value = "DOWNLOAD" id = "videofilebutton"
                 disabled={buttonDisabled}
                 className={buttonDisabled ? 'disabled' : ''}/>
        </form>
        <ResultIndicator result={this.state.result}
                         resultDetail={this.state.resultDetail}
                         parent={'video'} />
      </MuiThemeProvider>
    )
  }
}

export default VideoFileForm;
