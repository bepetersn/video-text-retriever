import React, { Component } from 'react';
import triggerFileDownload from 'js-file-download';
import contentDisposition from 'content-disposition';

import SubtitleFormatInput from './subtitleFormatInput.js';
import LanguageOptions from './languageOptions.js';
import LoadingIndicator from './loadingIndicator.js';
import ResultIndicator from './resultIndicator.js';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const AUDIO_API = '/api/download/audio'

const theme = createMuiTheme({
  palette: {
    primary: { main: '#212529' },
    secondary: { main: '#FFBD54' },
  },
});

class AudioForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {audioFileFormat: 'mp3'};
    this.handleAudioFileFormatChange = this.handleAudioFileFormatChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(e){
     e.preventDefault();
     const data = new FormData(e.target);
     this.setState({ loading: true, result: null });
     this.props.setLoad(true);
     fetch(AUDIO_API, {
      method: 'POST',
      body: data
    }).then(response => {
      if(response.ok) {
        const disposition = contentDisposition.parse(response.headers.get('content-disposition'));
        const filename = disposition.parameters.filename
        response.blob().then((content) => {
          this.setState({ loading: false });
          this.props.setLoad(false);
          triggerFileDownload(
            content, filename,
            filename.endsWith('mp3') ? "audio/mp3" :
            filename.endsWith('wav') ? "audio/wav" :
            "audio/flac"
            );
        });
      } else if(response.status === 429) {
        console.log('rate limited')
        response.text().then((text) => {
          const parser = new DOMParser();
          const htmlDoc = parser.parseFromString(text, 'text/html');
          const threshold = htmlDoc.querySelector('p').textContent;
          this.setState({ loading: false,
                          result: 'rate-limited',
                          resultDetail: threshold });
          this.props.setLoad(false);
        });
      } else {
        console.log('failed');
        this.setState({ loading: false, result: 'failed', resultDetail: null });
        this.props.setLoad(false);
      }
    }).catch(response => {
      console.log('connection error');
      this.setState({ loading: false, result: 'failed', resultDetail: null });
      this.props.setLoad(false);
    });
  }

  handleAudioFileFormatChange(e) {
    this.setState({ audioFileFormat: e.target.value });
  }

  render() {
    const isMp3Checked = this.state.audioFileFormat == 'mp3' ? true : false;
    const isWavChecked = this.state.audioFileFormat == 'wav' ? true : false;
    const isFlacChecked = this.state.audioFileFormat == 'flac' ? true : false;
    const buttonDisabled = this.props.buttonDisabled ? true : false;
    return (
      <MuiThemeProvider theme = {theme}>
        <LoadingIndicator loading={this.state.loading} tabValue = {this.props.tabValue} />
        <form id='audio-form' onSubmit={this.onSubmit}>
           <TextField
              id="audio-videourl"
              className="videoUrl"
              label="Enter Video URL"
              type="url"
              margin="normal"
              variant="outlined"
              name="video_url"
              onChange={this.props.handleVideoURLChange}
              style = {{width: '50%',background:"white",borderRadius:"5px"}}

           />
          <FormLabel component="legend" id = "filetypelabel">Choose a File Type</FormLabel>
          <RadioGroup id = "filetypeselect" row name="audio_format" style ={{justifyContent: "center"}}>
            <FormControlLabel
                  value="mp3"
                  control={<Radio color="primary" />}
                  checked={isMp3Checked}
                  onChange={this.handleAudioFileFormatChange}
                  label=".MP3"
                  labelPlacement="bottom"
                  color="primary"
            />
            <FormControlLabel
                  value="wav"
                  control={<Radio color="primary" />}
                  checked={isWavChecked}
                  onChange={this.handleAudioFileFormatChange}
                  label=".WAV"
                  labelPlacement="bottom"
                  color="primary"
            />
            <FormControlLabel
                  value="flac"
                  control={<Radio color="primary" />}
                  checked={isFlacChecked}
                  onChange={this.handleAudioFileFormatChange}
                  label=".FLAC"
                  labelPlacement="bottom"
                  color="primary"
            />
          </RadioGroup>
          <i className="fas fa-arrow-alt-circle-down" id = "downloadicon" />
          <input role="button" type="submit" value = "DOWNLOAD" id = "audiodownloadbutton"
                 disabled={buttonDisabled}
                 className={buttonDisabled ? 'disabled' : ''}/>
        </form>
        <ResultIndicator result={this.state.result}
                         resultDetail={this.state.resultDetail}
                         parent={'audio'} />
      </MuiThemeProvider>
    )
  }
}

export default AudioForm;
