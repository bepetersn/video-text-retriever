
import React, { useEffect, useState } from 'react';
import Hero from './hero.js';

const VERSION_API = '/api/version/';
const LIGHT_BLUE = '#007bffd1';
const WARNING = 'rgb(255, 129, 0)';
const DANGER = 'rgba(255, 0, 0, 0.82)';

// utils ///
const parseDate = (datestring, sep) => {
  var parts = datestring.split(sep);
  return new Date(
    Date.UTC(parts[0], parts[1]-1, parts[2]) // NOTE: month needs to be one less
  );
}
const dayDiff = (date1, date2) => Math.floor((date2 - date1) / (1000*60*60*24));
////////////

const App = (props) => {

  let [version, setVersion] = useState(0);
  let [daysSinceUpdate, setDaysSinceUpdate] = useState(0);

  useEffect(() => {
    /* Get the version & calculate days since release */
    fetch(`${origin}${VERSION_API}`).then(function(resp) {
      resp.json().then(function(result) {
        setVersion(result.youtube_dl);
        setDaysSinceUpdate(
          dayDiff(parseDate(result.youtube_dl, '.'), new Date()));
      });
    });
  });

  let versionExplanation = version ? (daysSinceUpdate +
                               ' days since last update') : ''
  let versionStyle = daysSinceUpdate < 30 ? LIGHT_BLUE :
                     daysSinceUpdate < 90 ? WARNING :
                                            DANGER;

  return (
    <div id="content" className="container">
      <Hero />
      <div id="footer">
        <a className="footerBadge" href= "../about.html" >
          <div className = "aboutlink">Tutorial</div>
        </a>
        <div className="footerBadge" id="version"
             title={versionExplanation}
             style={{backgroundColor: versionStyle,
                     display: "none"}}>
          {'ytdl: v' + version}
        </div>
      </div>
    </div>
  );
}

export default App
