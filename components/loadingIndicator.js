
import React, { Component } from 'react';

class LoadingIndicator extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    if(this.props.loading) {
      if (this.props.tabValue == 0){
          return (
            <div id="loading-indicator" className = "loadvisible">
              Converting Audio File
            </div>
          )
      }
      else if (this.props.tabValue == 1){
          return (
            <div id="loading-indicator" className = "loadvisible">
              Loading Subtitles
            </div>
          )
      }
      else if (this.props.tabValue == 2){
          return (
            <div id="loading-indicator" className = "loadvisible">
               Converting Video File
            </div>
          )
      } 

    }
    else {
        return (
          <div id="loading-indicator" className = "loadhidden">
            Loading Language Options
          </div>
        )
    }
  }
}

export default LoadingIndicator;
