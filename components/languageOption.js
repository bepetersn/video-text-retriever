
import React, { Component } from 'react';

class LanguageOption extends Component {
  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
  	this.props.onClick(e, this.props.data.code + this.props.data.type);
  }

  render() {
    const optionId = this.props.data.code + '-' + this.props.data.type;
  	return (

  	  <li key = {optionId} onClick={this.onClick}
  	  	  className={this.props.className}>
  	  	{this.props.data.names[0]} <span className = "langcode" >
  	  		({this.props.data.code})
  	  	</span>
        {this.props.clicked && <input type = "hidden" value = {this.props.clicked ? optionId : ""} 
               name = 'selected_lang' />}
  	  </li>
  	)
  }
}	

export default LanguageOption;
