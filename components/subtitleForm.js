
import React, { Component } from 'react';
import { camelize } from 'inflected';
import triggerFileDownload from 'js-file-download';
import contentDisposition from 'content-disposition';

import SubtitleFormatInput from './subtitleFormatInput.js';
import LanguageOptions from './languageOptions.js';
import LoadingIndicator from './loadingIndicator.js';
import ResultIndicator from './resultIndicator.js';

import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormLabel from '@material-ui/core/FormLabel';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const SUBTITLES_API = '/api/download/subtitles'
const SUBTITLES_API_OPTIONS = '/api/get/subtitles/options'


const theme = createMuiTheme({
  palette: {
    primary: { main: '#212529' },
    secondary: { main: '#FFBD54' },
  },
});

class SubtitleForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      videoUrl: '', loading: false,
      result: null, resultDetail: null,
      captionOptions: [], buttonDisabled: true,
      autoOptionsExpanded: null, subtitleFormat: 'vtt',
      optionClicked: ""
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onOptionClick = this.onOptionClick.bind(this);
    this.onAutoOptionsExpanderClick = this.onAutoOptionsExpanderClick.bind(this);
  }

  onAutoOptionsExpanderClick() {
    this.setState((prevState) => {
      return {autoOptionsExpanded: !prevState.autoOptionsExpanded };
    });
  }

  onOptionClick(e, prevOptionKey) {
    this.setState((prevState) => {
      console.log('on option click');
      const firstClick = prevState.optionClicked != prevOptionKey;
      return {
        optionClicked: firstClick ? prevOptionKey : "",
        buttonDisabled: firstClick ? false : true
      };
    });
  }

  onChange(e) {
    console.log(`${camelize(e.target.name, false)} changed`);
    let targetName = camelize(e.target.name, false);
    if(targetName == 'videoUrl' && e.target.value === '') {
      // TODO: check if the video url is valid
      return;
    }
    this.props.setLoad(true);
    this.setState({
      loading: true, captionOptions: null, buttonDisabled: true,
      result: null, [targetName]: e.target.value
    }, () => {
      // Get video subtitle options for video and format
      const data = {video_url: this.state.videoUrl,
                    subtitle_format: this.state.subtitleFormat};
      fetch(SUBTITLES_API_OPTIONS, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {

        // Load the received caption options
        if(response.ok) {
          response.json().then((captionOptions) => {

            if(captionOptions.length == 0) {
              this.setState({ result: 'no-options', loading: false });
              this.props.setLoad(false);
            } else {
              this.props.setLoad(false);
              this.setState({ captionOptions: captionOptions, loading: false},
                () => {
                  // Set the autocaptions box to be expanded or not
                  if (this.state.captionOptions){
                    const manualCaptionOptions = this.state.captionOptions.filter(
                      (language) => language.type !== "auto");
                    if (manualCaptionOptions.length == 0){
                      this.setState({ autoOptionsExpanded: true });
                    }
                    else{
                      this.setState({ autoOptionsExpanded: false });
                    }
                  }
              });
            }
          });
        } else if(response.status === 429) {
          console.log('rate limited')
          response.text().then((text) => {
            const parser = new DOMParser();
            const htmlDoc = parser.parseFromString(text, 'text/html');
            const threshold = htmlDoc.querySelector('p').textContent;
            this.setState({ loading: false,
                            result: 'rate-limited',
                            resultDetail: threshold });
            this.props.setLoad(false);
          });
        }
        else {
          console.log(response.status)
          console.log('failed');
          this.setState({ loading: false, result: 'failed', resultDetail: null});
          this.props.setLoad(false);
        }
      }).catch(response => {
        console.log('connection error');
        this.setState({ loading: false, result: 'failed', resultDetail: null});
        this.props.setLoad(false);
      });
    });
  }

  onSubmit(e) {
    e.preventDefault();
    const data = new FormData(e.target);
    this.setState({ loading: true, result: null });
    this.props.setLoad(true);
    fetch(SUBTITLES_API, {
      method: 'POST',
      body: data
    }).then(response => {
      const disposition = contentDisposition.parse(response.headers.get('content-disposition'));
      const filename = disposition.parameters.filename
      if(response.ok) {
        response.text().then((content) => {
          this.setState({ loading: false, result: null });
          this.props.setLoad(false);
          triggerFileDownload(
            content, filename,
            filename.endsWith('vtt') ? "text/vtt" : "text/srt");
        });
      } else {
        console.log('failed');
        this.setState({ loading: false, result: 'failed' });
        this.props.setLoad(false);
      }
    }).catch(response => {
      console.log('connection error');
      this.setState({ loading: false, result: 'connection-error' });
      this.props.setLoad(false);
    });
  }

  render() {
    const isVttChecked = this.state.subtitleFormat == 'vtt' ? true : false;
    const isSrtChecked = this.state.subtitleFormat == 'srt' ? true : false;
    const buttonDisabled = this.state.buttonDisabled ? true : '';

    return (
      <MuiThemeProvider theme = {theme}>
        <LoadingIndicator loading={this.state.loading} tabValue = {this.props.tabValue} />
        <form id='subtitle-form' onSubmit={this.onSubmit}
                              onChange={this.onChange}>
          <TextField
              id="subtitle-videourl"
              className="videoUrl"
              label="Enter Video URL"
              type="url"
              margin="normal"
              variant="outlined"
              name="video_url"
              style = {{width: '50%',background:"white",borderRadius:"5px"}}
           />
          <FormLabel component="legend" id = "filetypelabel">Choose a File Type</FormLabel>
          <RadioGroup id = "filetypeselect" row name="subtitle_format" style ={{justifyContent: "center"}}>
            <FormControlLabel
                  value="vtt"
                  control={<Radio color="primary" />}
                  checked={isVttChecked}
                  label=".VTT"
                  labelPlacement="bottom"
                  color="primary"
            />
            <FormControlLabel
                  value="srt"
                  control={<Radio color="primary" />}
                  checked={isSrtChecked}
                  label=".SRT"
                  labelPlacement="bottom"
                  color="primary"
            />
          </RadioGroup>
          <LanguageOptions languages={this.state.captionOptions}
                           expanded = {this.state.autoOptionsExpanded}
                           clickExpand={this.onAutoOptionsExpanderClick}
                           optionClicked={this.state.optionClicked}
                           onOptionClick={this.onOptionClick} />
          <i className="fas fa-arrow-alt-circle-down" id = "downloadicon" />
          <input role="button" type="submit" value = "DOWNLOAD" id = "subtitledownloadbutton"
                 disabled={buttonDisabled}
                 className={buttonDisabled ? 'disabled' : ''}/>
        </form>
        <ResultIndicator result={this.state.result}
                         resultDetail={this.state.resultDetail}
                         parent={'subtitle'} />
      </MuiThemeProvider>
    )
  }
}

export default SubtitleForm;
