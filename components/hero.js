
import React, { Component } from 'react';

import SubtitleForm from './subtitleForm.js';
import AudioForm from './audioForm.js';
import VideoFileForm from './videoFileForm.js';

import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import SwipeableViews from 'react-swipeable-views';


const theme = createMuiTheme({
  palette: {
    primary: { main: '#212529' },
    secondary: { main: '#FFBD54' },
  },
});

class Hero extends Component {

  constructor(props) {
    super(props);
    this.state = { tabValue: 1 , load:false };
    this.handleTabChange = this.handleTabChange.bind(this);
    this.setLoad = this.setLoad.bind(this);
  }

  handleTabChange(e, newVal){
    this.setState({ tabValue: newVal });
  }
  setLoad(value){
    this.setState({ load: value });
  }

  render() {
    return (
      <div className="jumbotron">
        <i className={this.state.load ? "fas fa-cog fa-spin" : "fas fa-dog"} id="dogicon"></i>
        <h2>Video Text Retriever</h2>
        <MuiThemeProvider theme = {theme}>
          <Paper square>
            <Tabs
              value = {this.state.tabValue}
              indicatorColor="primary"
              textColor="primary"
              centered
              onChange = {this.handleTabChange}
            >
              <Tab label="Audio"  disableRipple={true} />
              <Tab label="Text" disableRipple={true} />
              <Tab label="Video" disableRipple={true} />
            </Tabs>
          </Paper>
        </MuiThemeProvider>
        <SwipeableViews
          axis='x'
          index={this.state.tabValue}
        >
            <AudioForm tabValue={this.state.tabValue} setLoad={this.setLoad}/>
            <SubtitleForm tabValue={this.state.tabValue} setLoad={this.setLoad}/>
            <VideoFileForm tabValue={this.state.tabValue} setLoad={this.setLoad}/>
        </SwipeableViews>
      </div>
    )
  }
}

export default Hero;
