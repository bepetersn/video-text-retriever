
import React, { Component } from 'react';
import LanguageOption from './languageOption.js';
import Collapse from '@material-ui/core/Collapse';

class LanguageOptions extends Component {

  constructor(props) {
    super(props);
    this.forEachLanguage = this.forEachLanguage.bind(this);
  }

  forEachLanguage(language, index) {
      const key = language.code + language.type;
      return (
        <LanguageOption key={key} data={language}
          onClick={this.props.onOptionClick} index={index}
          clicked={this.props.optionClicked.includes(key) ? true : false}
          className={this.props.optionClicked.includes(key) ? 'clicked' : ''} />
      );
  }

  render() {
    if(this.props.languages) {
      const languages = this.props.languages;
      const autoLanguages = languages.filter((language) => {
        return language.type === "auto"
      });
      const showAuto = autoLanguages.length > 0;
      const autolistItems = autoLanguages.map(this.forEachLanguage);
      const manualLanguages = languages.filter((language) => {
        return language.type !== "auto"
      });
      const showManual = manualLanguages.length > 0;
      const manuallistItems = manualLanguages.map(this.forEachLanguage);

      return (
          <div>
            {showManual && <h4> Author-Provided Text </h4>}
            {showManual && <ul className = "languagelist">{manuallistItems}</ul>}

            {showAuto && <h4 id = "autocaptionheader"
                             onClick={this.props.clickExpand}>
                           Auto-Generated Text <i className={
                                  this.props.expanded ?
                                  "fas fa-chevron-up" : "fas fa-chevron-down"}>
                           </i>
                         </h4>}
            {showAuto && <Collapse in={this.props.expanded} timeout="auto"
                                   unmountOnExit>
                           <ul className = "languagelist "
                               id = 'autolist'>
                             {autolistItems}
                           </ul>
                         </Collapse>}

          </div>
      );
    }

    else {
      return null;
    }
  }
}

export default LanguageOptions;
