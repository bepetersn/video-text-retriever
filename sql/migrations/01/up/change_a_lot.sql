
alter table logs rename to request_logs;

alter table request_logs drop column content;
alter table request_logs drop column level;
alter table request_logs drop column status;

alter table request_logs add column media_type varchar(100);
alter table request_logs add column url varchar(512);
alter table request_logs add column format varchar(32);
alter table request_logs add column host varchar(256);

