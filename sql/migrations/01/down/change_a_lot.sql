
alter table request_logs rename to logs;

alter table logs add column content text;
alter table logs add column level varchar(32);
alter table logs add column status int;

alter table logs drop column media_type;
alter table logs drop column url;
alter table logs drop column format;
alter table logs drop column host;
